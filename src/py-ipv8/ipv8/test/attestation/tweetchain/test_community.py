from ....attestation.tweetchain.community import TweetChainCommunity
from ....attestation.tweetchain.community import EMPTY_HASH, GENESIS_HASH
from ...attestation.trustchain.test_block import TestBlock
from ...base import TestBase
from ...mocking.ipv8 import MockIPv8
from ...util import twisted_wrapper


class TestTweetChainCommunity(TestBase):

    def setUp(self):
        super(TestTweetChainCommunity, self).setUp()
        self.initialize(TweetChainCommunity, 4)

    def create_node(self):
        return MockIPv8(u"curve25519", TweetChainCommunity, working_directory=u":memory:")

    @twisted_wrapper
    def test_simple(self):
        yield self.introduce_nodes()

        # Create two subsequence messages by one user and check if hashes
        # are indeed correct.
        a1 = yield self.nodes[0].overlay.sign_block(
                transaction=dict(msg="First message"))

        a2 = yield self.nodes[0].overlay.sign_block(
                transaction=dict(msg="Second message"))

        self.assertEqual(a1.hash, a2.previous_hash)
        self.assertEqual(a1.previous_hash, GENESIS_HASH)

    @twisted_wrapper
    def test_linked(self):
        yield self.introduce_nodes()

        # Create three blocks by two users in the following
        # fashion and check if hashes are correct:
        #
        # User A  "First message", "Links to second message"
        #                ^             /
        #                |            /
        #                |           V
        # User B  "Links to first message"
        a1 = yield self.nodes[0].overlay.sign_block(
                transaction=dict(msg="First message"))

        b1 = yield self.nodes[1].overlay.sign_block(
                transaction=dict(msg="Links to first message"),
                linked=a1)

        a2 = yield self.nodes[0].overlay.sign_block(
                transaction=dict(msg="Links to second message"),
                linked=b1)

        self.assertEqual(a1.link_hash, EMPTY_HASH)
        self.assertEqual(a1.previous_hash, GENESIS_HASH)
        self.assertEqual(b1.link_hash, a1.hash)
        self.assertEqual(b1.previous_hash, GENESIS_HASH)
        self.assertEqual(a2.link_hash, b1.hash)
        self.assertEqual(a2.previous_hash, a1.hash)

    @twisted_wrapper
    def test_detect_cheater(self):
        yield self.introduce_nodes()

        # Create two messages by different users where the first links to
        # to the second. Check if the hash indeed invalidates if 
        # user A later decided to change its message.
        a1 = yield self.nodes[0].overlay.sign_block(
                transaction=dict(msg="First message"))

        b1 = yield self.nodes[1].overlay.sign_block(
                transaction=dict(msg="Links to first message"),
                linked=a1)

        self.assertEqual(a1.hash, b1.link_hash)

        a1.transaction['msg'] = "Another first message!"

        self.assertNotEqual(a1.hash, b1.link_hash)

    @twisted_wrapper
    def test_reputation_simple(self):
        yield self.introduce_nodes()

        a1 = yield self.nodes[0].overlay.sign_block(
            transaction=dict(msg="First message"))
        yield self.deliver_messages()

        # We expect this node to have a trust of 1 (no incoming links)
        self.assertAlmostEqual(self.nodes[0].overlay.calculate_reputation(a1), 1)

        a2 = yield self.nodes[0].overlay.sign_block(
            transaction=dict(msg="Links to first message"),
            linked=a1)
        yield self.deliver_messages()

        # Reputation should be 1 since we do not allow cheating by just adding blocks to yourself.
        self.assertAlmostEqual(self.nodes[0].overlay.calculate_reputation(a1), 1)

        b1 = yield self.nodes[1].overlay.sign_block(
            transaction=dict(msg="Also links to first message"),
            linked=a1)
        yield self.deliver_messages()

        # We expect reputation of 1 for block "a1" from the perspective of both users.
        self.assertAlmostEqual(self.nodes[0].overlay.calculate_reputation(a1), 1.9)
        self.assertAlmostEqual(self.nodes[1].overlay.calculate_reputation(a1), 1.9)

        # We expect reputation of 1.9 for block "b1" from the perspective of both users.
        self.assertAlmostEqual(self.nodes[0].overlay.calculate_reputation(b1), 1.0)
        self.assertAlmostEqual(self.nodes[1].overlay.calculate_reputation(b1), 1.0)

    @twisted_wrapper
    def test_reputation_complex(self):
        yield self.introduce_nodes()

        b1 = yield self.nodes[1].overlay.sign_block(transaction=dict())
        b2 = yield self.nodes[1].overlay.sign_block(transaction=dict())
        b3 = yield self.nodes[1].overlay.sign_block(transaction=dict())
        b4 = yield self.nodes[1].overlay.sign_block(transaction=dict())
        b5 = yield self.nodes[1].overlay.sign_block(transaction=dict())

        a1 = yield self.nodes[0].overlay.sign_block(transaction=dict())
        a2 = yield self.nodes[0].overlay.sign_block(transaction=dict(), linked=b5)
        a3 = yield self.nodes[0].overlay.sign_block(transaction=dict(), linked=b4)
        a4 = yield self.nodes[0].overlay.sign_block(transaction=dict())
        a5 = yield self.nodes[0].overlay.sign_block(transaction=dict(), linked=b2)

        c1 = yield self.nodes[2].overlay.sign_block(transaction=dict())
        c2 = yield self.nodes[2].overlay.sign_block(transaction=dict(), linked=b1)
        c3 = yield self.nodes[2].overlay.sign_block(transaction=dict(), linked=a1)
        c4 = yield self.nodes[2].overlay.sign_block(transaction=dict(), linked=b3)
        c5 = yield self.nodes[2].overlay.sign_block(transaction=dict(), linked=b2)

        d1 = yield self.nodes[3].overlay.sign_block(transaction=dict())
        d2 = yield self.nodes[3].overlay.sign_block(transaction=dict(), linked=c1)
        d3 = yield self.nodes[3].overlay.sign_block(transaction=dict(), linked=b2)
        d4 = yield self.nodes[3].overlay.sign_block(transaction=dict(), linked=a4)
        d5 = yield self.nodes[3].overlay.sign_block(transaction=dict())

        yield self.deliver_messages()

        # correct scores according to the reputation formula.
        check_scores = [
                (a1, 1.9), (a2, 1.0), (a3, 1.0), (a4, 1.9), (a5, 1.0),
                (b1, 1.9), (b2, 3.7), (b3, 1.9), (b4, 1.9), (b5, 1.9),
                (c1, 1.9), (c2, 1.0), (c3, 1.0), (c4, 1.0), (c5, 1.0),
                (d1, 1.0), (d2, 1.0), (d3, 1.0), (d4, 1.0), (d5, 1.0)]
        
        for block, score in check_scores:
            self.assertAlmostEqual(self.nodes[0].overlay.calculate_reputation(block), score)


    @twisted_wrapper
    def test_search(self):
        a1 = yield self.nodes[0].overlay.sign_block(
                transaction=dict(msg="First message"))

        b1 = yield self.nodes[1].overlay.sign_block(
                transaction=dict(msg="Links to first message"),
                linked=a1)

        self.assertAlmostEqual(self.nodes[0].overlay.calculate_reputation(a1), 1.0)

        yield self.introduce_nodes()
        yield self.deliver_messages()

        self.nodes[0].overlay.search_linked(a1)
        yield self.deliver_messages()

        self.assertAlmostEqual(self.nodes[0].overlay.calculate_reputation(a1), 1.9)
