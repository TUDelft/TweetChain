"""
The TweetChain Community is the first step in an incremental approach in building a new reputation system.
This reputation system builds a tamper proof interaction history contained in a chain data-structure.
Every node has a chain and these chains intertwine by blocks shared by chains.
"""
import logging
import random
from threading import RLock

from twisted.internet import reactor
from twisted.internet.defer import Deferred, succeed, fail

from .block import TweetChainBlock, ValidationResult, EMPTY_PK, GENESIS_SEQ, UNKNOWN_SEQ, EMPTY_HASH, GENESIS_HASH
from .caches import CrawlRequestCache
from .database import TweetChainDB
from ...deprecated.community import Community
from ...deprecated.payload import IntroductionResponsePayload
from ...deprecated.payload_headers import BinMemberAuthenticationPayload, GlobalTimeDistributionPayload
from .payload import *
from ...peer import Peer
from ...requestcache import RandomNumberCache, RequestCache

HALF_BLOCK = u"half_block"
CRAWL = u"crawl"
receive_block_lock = RLock()


def synchronized(f):
    """
    Due to database inconsistencies, we can't allow multiple threads to handle a received_half_block at the same time.
    """
    def wrapper(self, *args, **kwargs):
        with receive_block_lock:
            return f(self, *args, **kwargs)
    return wrapper


class TweetChainCommunity(Community):
    """
    Community for reputation based on TweetChain tamper proof interaction history.
    """
    BLOCK_CLASS = TweetChainBlock
    DB_CLASS = TweetChainDB
    DB_NAME = 'tweetchain'
    BROADCAST_FANOUT = 10
    version = '\x02'
    master_peer = Peer(("3081a7301006072a8648ce3d020106052b81040027038192000403428b0fa33d3ed62dd39852481f535e2161714" +
                        "4a95e682ad5733b9a739b27051dc6ad1da743a463821fc8d3d1849191d5fb84fab1f3fe3ad44fb2b83f07d0c78a" +
                        "13b7ad1d311063069f49070cad7dc15620996cdd625c1abcdbfabf750727f1dec706f6f16cb28ce6946fdf39887" +
                        "a84fc457a5f9edc660adbe0a72ea5219f9578dd6432de825c167e80987ca4c6a2bf").decode("HEX"))

    def __init__(self, *args, **kwargs):
        working_directory = kwargs.pop('working_directory', '')
        db_name = kwargs.pop('db_name', self.DB_NAME)
        super(TweetChainCommunity, self).__init__(*args, **kwargs)
        self.request_cache = RequestCache()
        self.logger = logging.getLogger(self.__class__.__name__)
        self.persistence = self.DB_CLASS(working_directory, db_name)
        self.relayed_broadcasts = []
        self.logger.debug("The tweetchain community started with Public Key: %s",
                          self.my_peer.public_key.key_to_bin().encode("hex"))
        self.broadcast_block = True  # Whether we broadcast a full block after constructing it

        self.decode_map.update({
            chr(1): self.received_half_block,
            chr(2): self.received_crawl_request,
            chr(3): self.received_crawl_response,
            chr(5): self.received_half_block_broadcast,
            chr(8): self.receive_search_linked,
        })

    def should_sign(self, block):
        """
        Return whether we should sign the block in the passed message.
        @param block: the block we want to sign or not.
        """
        return True

    def calculate_reputation(self, block):
        # Get blocks that link to the given block
        blocks = self.persistence.get_linked_incoming(block)

        # Remove blocks by same owner
        pub_key = block.public_key
        blocks = [x for x in blocks if x.public_key != pub_key]

        # Calculate reputation
        alpha = 0.9
        return 1 + alpha * sum(self.calculate_reputation(x) for x in blocks)

    def send_block(self, block, address=None, ttl=2):
        """
        Send a block to a specific address, or do a broadcast to known peers if no peer is specified.
        """
        global_time = self.claim_global_time()
        dist = GlobalTimeDistributionPayload(global_time).to_pack_list()

        if address:
            self.logger.debug("Sending block to (%s:%d) (%s)", address[0], address[1], block)
            payload = HalfBlockPayload.from_half_block(block).to_pack_list()
            packet = self._ez_pack(self._prefix, 1, [dist, payload], False)
            self.endpoint.send(address, packet)
        else:
            self.logger.debug("Broadcasting block %s", block)
            payload = HalfBlockBroadcastPayload.from_half_block(block, ttl).to_pack_list()
            packet = self._ez_pack(self._prefix, 5, [dist, payload], False)
            for peer in random.sample(self.network.verified_peers, min(len(self.network.verified_peers),
                                                                       self.BROADCAST_FANOUT)):
                self.endpoint.send(peer.address, packet)
            self.relayed_broadcasts.append(block.block_id)

    def sign_block(self, transaction, linked=None):
        """
        Create, sign, persist and send a block signed message
        :param transaction: A string describing the interaction in this block
        :param linked: Optional, the block that this block links to.
        """
        assert transaction is None or isinstance(transaction, dict), "Transaction should be a dictionary"

        block = self.BLOCK_CLASS.create(transaction, self.persistence, self.my_peer.public_key.key_to_bin(),
                                        link=linked)
        block.sign(self.my_peer.key)
        validation = block.validate(self.persistence)
        self.logger.info("Signed block to %s (%s) validation result %s",
                         block.link_public_key.encode("hex")[-8:], block, validation)
        if validation[0] != ValidationResult.partial_next and validation[0] != ValidationResult.valid:
            self.logger.error("Signed block did not validate?! Result %s", repr(validation))
            return fail(RuntimeError("Signed block did not validate."))

        if not self.persistence.contains(block):
            self.persistence.add_block(block)

        # Also add the linked block so we know about it
        if linked and not self.persistence.contains(linked):
            self.persistence.add_block(linked)

        # Broadcast block to peers.
        self.send_block(block)

        return succeed(block)


    @synchronized
    def received_half_block(self, source_address, data):
        """
        We've received a half block, either because we sent a SIGNED message to some one or we are crawling
        """
        dist, payload = self._ez_unpack_noauth(HalfBlockPayload, data)
        block = TweetChainBlock.from_payload(payload, self.serializer)
        validation = self.validate_persist_block(block)

    @synchronized
    def received_half_block_broadcast(self, source, data):
        """
        We received a half block, part of a broadcast. Disseminate it further.
        """
        dist, payload = self._ez_unpack_noauth(HalfBlockBroadcastPayload, data)
        block = TweetChainBlock.from_payload(payload, self.serializer)
        self.validate_persist_block(block)

        if block.block_id not in self.relayed_broadcasts and payload.ttl > 0:
            self.send_block(block, ttl=payload.ttl - 1)

    @synchronized
    def validate_persist_block(self, block):
        """
        Validate a block and if it's valid, persist it. Return the validation result.
        :param block: The block to validate and persist.
        :return: [ValidationResult]
        """
        validation = block.validate(self.persistence)
        self.logger.debug("Block validation result %s, %s, (%s)", validation[0], validation[1], block)

        if validation[0] == ValidationResult.invalid:
            return
        elif not self.persistence.contains(block):
            self.persistence.add_block(block)

        return validation

    def crawl_lowest_unknown(self, peer):
        """
        Crawl the lowest unknown block of a specific peer.
        """
        sq = self.persistence.get_lowest_sequence_number_unknown(peer.public_key.key_to_bin())
        return self.send_crawl_request(peer, peer.public_key.key_to_bin(), sequence_number=sq)

    def send_crawl_request(self, peer, public_key, sequence_number=None, for_half_block=None):
        """
        Send a crawl request to a specific peer.
        """
        sq = sequence_number
        if sequence_number is None:
            blk = self.persistence.get_latest(public_key)
            sq = blk.sequence_number if blk else GENESIS_SEQ
        sq = max(GENESIS_SEQ, sq) if sq >= 0 else sq

        crawl_id = for_half_block.hash_number if for_half_block else \
            RandomNumberCache.find_unclaimed_identifier(self.request_cache, u"crawl")
        crawl_deferred = Deferred()
        self.request_cache.add(CrawlRequestCache(self, crawl_id, crawl_deferred))
        self.logger.info("Requesting crawl of node %s:%d with id %d", public_key.encode("hex")[-8:], sq, crawl_id)

        global_time = self.claim_global_time()
        auth = BinMemberAuthenticationPayload(self.my_peer.public_key.key_to_bin()).to_pack_list()
        payload = CrawlRequestPayload(sq, crawl_id).to_pack_list()
        dist = GlobalTimeDistributionPayload(global_time).to_pack_list()

        packet = self._ez_pack(self._prefix, 2, [auth, dist, payload])
        self.endpoint.send(peer.address, packet)

        return crawl_deferred

    @synchronized
    def received_crawl_request(self, source_address, data):
        auth, dist, payload = self._ez_unpack_auth(CrawlRequestPayload, data)
        peer = Peer(auth.public_key_bin, source_address)

        self.logger.info("Received crawl request from node %s for sequence number %d",
                         peer.public_key.key_to_bin().encode("hex")[-8:],
                         payload.requested_sequence_number)
        sq = payload.requested_sequence_number
        if sq < 0:
            last_block = self.persistence.get_latest(self.my_peer.public_key.key_to_bin())
            # The -1 element is the last_block.seq_nr
            # The -2 element is the last_block.seq_nr - 1
            # Etc. until the genesis seq_nr
            sq = max(GENESIS_SEQ, last_block.sequence_number + (sq + 1)) if last_block else GENESIS_SEQ
        blocks = self.persistence.crawl(self.my_peer.public_key.key_to_bin(), sq)
        total_count = len(blocks)

        if total_count == 0:
            # If there are no blocks to send, send a dummy block back with an empty transaction.
            # This is to inform the requester that he can't expect any blocks.
            block = self.BLOCK_CLASS.create({}, self.persistence, self.my_peer.public_key.key_to_bin(),
                                            link_pk=EMPTY_PK)
            self.send_crawl_response(block, payload.crawl_id, 0, 0, peer)

        for ind in xrange(len(blocks)):
            self.send_crawl_response(blocks[ind], payload.crawl_id, ind + 1, total_count, peer)
        self.logger.info("Sent %d blocks", total_count)

    def send_crawl_response(self, block, crawl_id, index, total_count, peer):
        self.logger.debug("Sending block for crawl request to %s (%s)", peer, block)

        global_time = self.claim_global_time()
        payload = CrawlResponsePayload.from_crawl(block, crawl_id, index, total_count).to_pack_list()
        dist = GlobalTimeDistributionPayload(global_time).to_pack_list()

        packet = self._ez_pack(self._prefix, 3, [dist, payload], False)
        self.endpoint.send(peer.address, packet)

    @synchronized
    def received_crawl_response(self, source_address, data):
        dist, payload = self._ez_unpack_noauth(CrawlResponsePayload, data)
        self.received_half_block(source_address, data[:-12])  # We cut off a few bytes to make it a BlockPayload

        block = TweetChainBlock.from_payload(payload, self.serializer)
        cache = self.request_cache.get(u"crawl", payload.crawl_id)
        if cache:
            cache.received_block(block, payload.total_count)

    def search_linked(self, block):
        '''
        Broadcast a search request over the network indicating that the current
        node is searching for blocks that link to the given block.
        '''
        self.broadcast_search_linked(
                self.my_peer.address,
                block.public_key,
                block.sequence_number,
                block.hash)

    def broadcast_search_linked(self, source_address, pub_key, seq_number, block_hash):
        '''
        Relay a search request over the network indicating that the some
        node is searching for blocks that link to the given block.

        :param source_address: The node that issued the search request.
        :param pub_key: Public key of the block.
        :param seq_number: Sequence number of the block.
        :param block_hash: Hash of the block.
        '''

        self.logger.debug("Broadcasting search request for block %s:%d for node %s",
                pub_key.encode('hex'), seq_number, source_address)

        global_time = self.claim_global_time()
        dist = GlobalTimeDistributionPayload(global_time).to_pack_list()
        auth = BinMemberAuthenticationPayload(self.my_peer.public_key.key_to_bin()).to_pack_list()

        payload = SearchIncomingPayload(
            pub_key, 
            seq_number, 
            block_hash).to_pack_list()
        packet = self._ez_pack(self._prefix, 8, [auth, dist, payload])

        # Broadcast packet to all peers.
        for peer in self.network.verified_peers:
            self.endpoint.send(peer.address, packet)

    @synchronized
    def receive_search_linked(self, source_address, data):
        '''
        Process an incoming search request indicating that some node in the
        network is searching blocks that link to the given block.
        '''
        auth, dist, payload = self._ez_unpack_auth(SearchIncomingPayload, data)
        peer = Peer(auth.public_key_bin, source_address)

        self.logger.debug("Received search request for block %s:%d for node %s",
                payload.public_key.encode('hex'), 
                payload.sequence_number, 
                source_address)

        # Get the root block of the search query.
        root_block = self.persistence.get(
                payload.public_key,
                payload.sequence_number)

        # Get known blocks that link to the root block.
        if root_block:
            if root_block.hash != payload.hash:
                logger.error("received search request for block %s but with "
                        "invalid hash %s, ignoring request.",
                        root_block, payload.hash.encode('hex'))
                return

            linked_blocks = self.persistence.get_linked_incoming(root_block)

            for block in linked_blocks:
                self.send_block(block, source_address)

    def get_trust(self, peer):
        """
        Return the trust score for a specific peer. For the basic Tweetchain, this is the length of their chain.
        """
        block = self.persistence.get_latest(peer.public_key.key_to_bin())
        if block:
            return block.sequence_number
        else:
            # We need a minimum of 1 trust to have a chance to be selected in the categorical distribution.
            return 1

    def get_peer_for_introduction(self, exclude=None):
        """
        Choose a trusted peer to introduce you to someone else.
        The more trust you have for someone, the higher the chance is to forward them.
        """
        eligible = [p for p in self.get_peers() if p != exclude]
        if not eligible:
            return None

        total_trust = sum([self.get_trust(peer) for peer in eligible])
        random_trust_i = random.randint(0, total_trust - 1)
        current_trust_i = 0
        for i in xrange(0, len(eligible)):
            next_trust_i = self.get_trust(eligible[i])
            if current_trust_i + next_trust_i > random_trust_i:
                return eligible[i]
            else:
                current_trust_i += next_trust_i

        return eligible[-1]

    def on_introduction_response(self, source_address, data):
        super(TweetChainCommunity, self).on_introduction_response(source_address, data)

        auth, _, _ = self._ez_unpack_auth(IntroductionResponsePayload, data)
        peer = Peer(auth.public_key_bin, source_address)
        self.crawl_lowest_unknown(peer)

    def unload(self):
        self.logger.debug("Unloading the TweetChain Community.")

        self.request_cache.shutdown()

        super(TweetChainCommunity, self).unload()

        # Close the persistence layer
        self.persistence.close()
