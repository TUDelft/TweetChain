\section{Example of Application}
\label{sec:application}

As blocks become more and more entangled such as shown in Figure~\ref{fig:tweetchain-network-structure}, one can envision many different applications that analyze and process the network.
Examples of these applications are calculating trustworthiness of agents, finding paths, detecting cliques, analyzing the spread of news, and checking plagiarism.
In this report, as a simple use-case, we propose an algorithm for one of such applications: calculating the \emph{reputation} of a publication.

The reputation of a publication indicates the \emph{importance} of the work within the network.
Let $b$ be a block and let $\mathcal{N}_\text{in}(b)$ be the set of blocks by other agents that cite block $b$ (i.e., the \emph{incoming} pointers).
A straightforward way to define the reputation of $b$ is simply as the number of incoming pointers, i.e., the size of $\mathcal{N}_\text{in}(b)$.
Intuitively, one can argue that as a block gains more and more incoming pointers, its reputation must accordingly.

However, this definition suffers from the drawback that it does not take into account the reputation of the blocks for the incoming pointers.
A block $x$ cited by few blocks all having a high reputation is equally important as a block $y$ cited by many blocks all having a low reputation.
The reputation of a block is thus determined by the reputation of the incoming pointers.
Thus, instead, we define the reputation of block $b$ using the following recursive equation.

\begin{equation}
\text{reputation}(b) = 1 + \alpha \sum_{b' \in \mathcal{N}_\text{in}(b)} \text{reputation}(b')
\end{equation}

The reputation of block $b$ is determined by the sum over the reputation of blocks that cite $b$.
The factor $\alpha \in [0, 1]$ is a constant that weighs the impact of the recursive term as the number of hops increases.
Directly linked blocks are weighed by $\alpha$, blocks which are linked to these linked blocks are weighed by $\alpha^2$, and so forth.
For our implementation, we have chosen $\alpha=0.9=1-0.1$, meaning the impact of the recursive term is reduced by 10\% for each level of the recursion. 